![](./static/logo-240.png)

**_Energy for city life, Energize smart living_**

---

<br>

# iOS Energy SDK

- [วิธีการติดตั้ง](#วิธีการติดตั้ง)
- [วิธีการเรียกใช้งาน](#วิธีการเรียกใช้งาน)
  - [Import SDK](#import-sdk)
  - [Initialization](#Initialization)
  - [แสดงข้อมูลจากการไฟฟ้านครหลวง](#แสดงข้อมูลจากการไฟฟ้านครหลวง)
    - [แสดงข้อมูลทั้งหมดของเครื่องวัดฯ](#แสดงข้อมูลทั้งหมดของเครื่องวัดฯ)
    - [แสดงค่าไฟฟ้าเดือนปัจจุบัน](#แสดงค่าไฟฟ้าเดือนปัจจุบัน)
    - [แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน](#แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง-6-เดือน)
    - [แสดงรายการแจ้งเตือน](#แสดงรายการแจ้งเตือน)
- [วิธีการลงทะเบียนเครื่องวัดฯ](#วิธีการลงทะเบียนเครื่องวัดฯ)
- [SDK Theme](#sdk-theme])
- [วิธีหาบัญชีแสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้า](#วิธีหาบัญชีแสดงสัญญา-ca-จาก-qr-code-ในบิลค่าไฟฟ้า)

<br>

# วิธีการติดตั้ง

1. ดาวน์โหลด ​SDK จาก https://gitlab.com/mea.energysdk/ios-energy-sdk/-/archive/master/ios-energy-sdk-master.zip?path=v1.3.0 และ unzip file
2. เพิ่มไฟล์ **MEAEnergyKit.framework** ลงใน Project ที่ต้องการใช้งาน SDK
3. Embed framework โดยการกำหนดค่าใน XCODE_PROJECT > Build Phases

![](./static/embed-framework.png)

> กรณีไม่มี section **Embed framework** ให้เพิ่ม section ดังกล่าว โดยการ คลิกปุ่ม **+** > New Copy Files Pahse > เลือก Destination เป็น **Frameworks**

4. เพิ่ม dependencies ใน **Podfile** ตามรายการด้านล่าง

```
target 'YOUR TARGET' do
  ...
  pod 'Alamofire', '5.2'
  pod 'SnapKit', '~> 5.0.0'
  pod 'ReactiveCocoa', '~> 10.1'
  pod 'Charts'
  ...
end
```

5. Run คำสั่ง `pod install`

<br/>

# วิธีการเรียกใช้งาน

### Import SDK

Import **MEAEnergyKit** ในไฟล์ที่ต้องการใช้งาน SDK

```swift
import MEAEnergyKit
```

### Initialization

Initialize SDK พร้อมกับกำหนด Environment, API Key และ Client Code ที่ได้รับจากการไฟฟ้านครหลวง ควรกำหนดตั้งแต่ที่ Application เริ่มทำงาน

```swift
MEAEnergy.setup()
MEAEnergy.setEnv("YOUR_ENV")
MEAEnergy.setAPIKey(key: "YOUR_KEY",
             clientCode: "YOUR_CLIENT_CODE")
```

สามารถกำหนด Environment ได้ดังนี้

```swift
.STAGING // Staging environment (default)
.PRODUCTION // Production environment

// Example
MEAEnergy.setEnv(.PRODUCTION)
```

## แสดงข้อมูลจากการไฟฟ้านครหลวง

> ทุกเครื่องวัดฯ ที่ต้องการใช้งาน จะต้อง [ลงทะเบียน](#วิธีการลงทะเบียนเครื่องวัดฯ) กับการไฟฟ้านครหลวงในครั้งแรกก่อนเริ่มต้นใช้งาน

<br/>

### แสดงข้อมูลทั้งหมดของเครื่องวัดฯ

เพิ่มการแสดงผล MEAEnergyViewController ใน ViewController ที่ต้องการ กำหนดบ้านหรือคอนโดที่ต้องการแสดงข้อมูล โดยกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```swift
let meaEnergyViewController = MEAEnergyViewController(caNumber: "Your CA",
                                                         theme: MEAThemeUtility(theme: .mea)){ [weak self]
                                                         (error) in
                                                                if let error = error {
                                                                  // perform error case
                                                                }
}
addChild(meaEnergyViewController)
view.addSubview(meaEnergyViewController.view)
meaEnergyViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(40)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
}
meaEnergyViewController.didMove(toParent: self)

// or
// present(meaEnergyViewController, animated: true, completion: nil)
```

<br/>

### แสดงค่าไฟฟ้าเดือนปัจจุบัน

เพิ่มการแสดงผล MEAEnergyBillingViewController ใน ViewController ที่ต้องการ และกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```swift
let billingViewController = MEAEnergyBillingViewController(caNumber: "Your CA",
                                                              theme: MEAThemeUtility(theme: .mea)) { [weak self] (error) in
                                                                if let error = error {
                                                                  // perform error case
                                                                }
}
addChild(billingViewController)
view.addSubview(billingViewController.view)
billingViewController.didMove(toParent: self)
```

<br/>

### แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน

เพิ่มการแสดงผล MEAEnergyConsumptionHistoryViewController ใน ViewController ที่ต้องการ และกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```swift
let cViewController = MEAEnergyConsumptionHistoryViewController(caNumber: "Your CA",
                                                                theme: MEAThemeUtility(theme: .mea)) { [weak self] (error) in
                                                                  if let error = error {
                                                                    // perform error case
                                                                  }
}
addChild(cViewController)
view.addSubview(cViewController.view)
cViewController.didMove(toParent: self)
```

<br/>

### แสดงรายการแจ้งเตือน

เพิ่มการแสดงผล MEAEnergyMessagingViewController ใน ViewController ที่ต้องการ และกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```swift
let messagingViewController = MEAEnergyMessagingViewController(caNumber: "Your CA",
                                                              theme: MEAThemeUtility(theme: .mea)) { [weak self] (error) in
                                                              if let error = error {
                                                                // perform error case
                                                              }
}
addChild(messagingViewController)
view.addSubview(messagingViewController.view)
messagingViewController.didMove(toParent: self)
```

<br/>

> ทุก ๆ Compoent สามารถเลือก theme ให้เหมาะสมแก่การใช้งานได้มากยิ่งขึ้น ศึกษาวิธีการเปลี่ยน theme ได้จาก [SDK Theme](#sdk-theme])

<br/>

# วิธีการลงทะเบียนเครื่องวัดฯ

> การลงทะเบียนเครื่องวัดฯ จะทำ **เฉพาะการเข้าใช้งานครั้งแรกของเครื่องวัดฯ** เท่านั้น

```swift
MEAEnergy.registerCA(caNumber: "Your CA", // เลขที่แสดงสัญญา (CA)
                     projectCode: "Project Code", // รหัสโครงการ
                     projectName: "Project Name", // ชื่อโครงการภาษาไทย
                     projectNameEn: "Project Name EN", // ชื่อโครงการภาษาอังกฤษ
                     projectAddress: "Project Address", // ที่อยู่โครงการภาษาไทย
                     projectAddressEn: "Project Address EN", // ที่อยู่โครงการภาษาอังกฤษ
                     ) { [weak self] (response, error) in
                          if let error = error {
                            // perform error case
                          } else if let response = response {
                            // perform success case
                          }
}
```

<br/>

# SDK Theme

```swift
.mea // MEA theme (default)
.light // Light theme
.dark // Dark theme

// Example
MEAEnergyViewController(caNumber: "Your CA",
                        theme: MEAThemeUtility(theme: .mea))

MEAEnergyViewController(caNumber: "Your CA",
                        theme: MEAThemeUtility(theme: .light))

MEAEnergyViewController(caNumber: "Your CA",
                        theme: MEAThemeUtility(theme: .dark))
```

![](./static/all-theme.jpg)

<br/>

# วิธีหาบัญชีแสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้า

MEA Energy SDK อำนวยความสะดวกให้ผู้ใช้งานในการค้นหาและเรียกใช้บัญชีแสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้าของการไฟฟ้านครหลวง โดยผู้ใช้งานส่ง String ของ QR Code ในบิลค่าไฟฟ้าเข้า Method `getCAFromQRCode()` ตัวอย่างเช่น

```swift
let ca =  MEAUtilities.getCAFromQRCode(qrCodeString: "Your QR Code String")
```

<br/>
<br/>
<br/>

![](./static/device-1.png)

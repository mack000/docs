# Android Energy SDK Reference

#### Class Reference

- [MEAEnergy](#class-meaenergy)
- [MEAEnergyError](#class-meaenergyerror)
- [MEAEnergyUtils](#class-meaenergyutils)
- [MEAEnergyWidget](#class-meaenergywidget)
- [MEAEnergyWidgetCallback](#class-MEAEnergyWidgetCallback)

---

## Class MEAEnergy

#### public static void setAPIKey(String key)

Initial API Key

- `key` API Key
- `clientCode` Client Code

#### public void registerCA(String ca, String projectCode, String projectName, String projectNameEn, String projectAddress, String projectAddressEn, MEAEnergyCallback callback)

Register CA

- `ca` เลขที่แสดงสัญญา (CA)
- `projectCode` รหัสโครงการ
- `projectName` ชื่อโครงการภาษาไทย
- `projectNameEn` ชื่อโครงการภาษาอังกฤษ
- `projectAddress` ที่อยู่โครงการภาษาไทย
- `projectAddressEn` ที่อยู่โครงการภาษาอังกฤษ
- `MEAEnergyCallback` MEAEnergyCallback
  - **onSuccess(JSONObject data)**
    - `data`
      - `projectId`
      - `meterNo`
  - **onFail(MEAEnergySDKError error)** - `error` - `code` รหัส error - `message` ข้อความ error

<br/>

## Class MEAEnergyError

- `code`
- `message`

<br/>

## Class MEAEnergyUtils

#### public static String getCAFromQRCode(String qrCodeString)

- `qrCodeString` QR Code string จากบิลค่าไฟฟ้า

  ##### Return

  - เลขบัญชีแสดงสัญญา (CA)

<br/>

## Class MEAEnergyWidget

#### public void initCA(String ca, MEAEnergyWidgetCallback callback)

- `ca` บัญชีแสดงสัญญาที่
- `callback` Callback object

<br/>

## Class MEAEnergyWidgetCallback

#### public void onDone()
#### public boid onFail(error: MEAEnergyError?)
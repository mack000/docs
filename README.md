![](./static/intro.png)

# MEA Energy SDK

- ### [Android](./android/readme.md) **_(v1.1.3)_**
- ### [iOS](./ios/readme.md) **_(v1.3.0)_**
- ### [React Native](./react-native/readme.md) **_(v1.1.0)_**

<br/>
<br/>

# ขั้นตอนการใช้งาน MEA Energy SDK

![](./static/flow.jpg)
